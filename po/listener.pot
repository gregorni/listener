# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the listener package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: listener\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-15 16:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Translate as "Listener"
#: data/io.gitlab.gregorni.Listener.desktop.in:4
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:7 src/main.py:85
#: src/gtk/window.blp:7
msgid "Listener"
msgstr ""

#: data/io.gitlab.gregorni.Listener.desktop.in:5
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:10
msgid "Talk without being judged"
msgstr ""

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.Listener.desktop.in:14
msgid "talk;rubber ducky;mental health;programming;"
msgstr ""

#. Translators: Leave this as is, or blank
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:9
msgid "gregorni"
msgstr ""

#: data/io.gitlab.gregorni.Listener.appdata.xml.in:13
msgid ""
"Listener displays a helpful smiley that you can talk to without being "
"judged. For less cheerful topics, you can change the smiley to look sad. To "
"programmers, Listener even provides a Rubber Ducky mode that forces you to "
"explain your bug in simple terms, making it easier to understand what the "
"actual problem is."
msgstr ""

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:30
msgid "talk"
msgstr ""

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:32
msgid "rubber ducky"
msgstr ""

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:34
msgid "mental health"
msgstr ""

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:36
msgid "programming"
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:54
msgid "Happy smiley"
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:59
msgid "Sad smiley"
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Listener.appdata.xml.in:64 src/gtk/window.blp:90
msgid "Rubber Ducky"
msgstr ""

#: data/io.gitlab.gregorni.Listener.appdata.xml.in:86
msgid "First Release! 🎉"
msgstr ""

#. Translators: Translate this string as your translator credits.
#. Name only:    gregorni
#. Name + URL:   gregorni https://gitlab.com/gregorni/
#. Name + Email: gregorni <gregorniehl@web.de>
#. Do not remove existing names.
#. Names are separated with newlines.
#: src/main.py:97
msgid "translator-credits"
msgstr ""

#: src/main.py:98
msgid "Copyright © 2023 Listener Contributors"
msgstr ""

#: src/main.py:106
msgid "Code and Design Borrowed from"
msgstr ""

#: src/gtk/window.blp:29
msgid "Toggle Mood"
msgstr ""

#: src/gtk/window.blp:67
msgid "Explain the Bug in Simple Terms"
msgstr ""

#: src/gtk/window.blp:98
msgid "_Keyboard Shortcuts"
msgstr ""

#: src/gtk/window.blp:103
msgid "_About Listener"
msgstr ""

#: src/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: src/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: src/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr ""
