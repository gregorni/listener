# window.py
#
# Copyright 2023 Listener Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gio, Gtk


@Gtk.Template(resource_path="/io/gitlab/gregorni/Listener/gtk/window.ui")
class ListenerWindow(Adw.ApplicationWindow):
    __gtype_name__ = "ListenerWindow"

    main_stack = Gtk.Template.Child()
    emotions_button = Gtk.Template.Child()
    emoji_image = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.Listener")
        settings.bind("width", self, "default-width", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("height", self, "default-height", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("is-maximized", self, "maximized", Gio.SettingsBindFlags.DEFAULT)

        if kwargs.get("application").settings.get_value("rubber-ducky"):
            self.main_stack.set_visible_child_name("rubber-ducky")

        def __on_button_clicked(*args):
            is_happy = (
                self.emoji_image.get_icon_name() == "sentiment-satisfied-symbolic"
            )
            self.emoji_image.set_icon_name("sentiment-satisfied-symbolic")
            if is_happy:
                self.emoji_image.set_icon_name("sentiment-dissatisfied-symbolic")

        self.emotions_button.connect("clicked", __on_button_clicked)
