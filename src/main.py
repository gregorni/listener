# main.py
#
# Copyright 2023 Listener Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

from gi.repository import Adw, Gio

from .window import ListenerWindow


class ListenerApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(
            application_id="io.gitlab.gregorni.Listener",
            flags=Gio.ApplicationFlags.DEFAULT_FLAGS,
        )
        self.__create_action("quit", lambda *_: self.quit(), ["<primary>q"])
        self.__create_action("about", self.__on_about_action)

        self.settings = Gio.Settings(schema_id="io.gitlab.gregorni.Listener")

        action = Gio.SimpleAction.new_stateful(
            "rubber-ducky", None, self.settings.get_value("rubber-ducky")
        )
        action.connect("change-state", self.__on_rubber_ducky_changed)
        self.add_action(action)

    def __on_rubber_ducky_changed(self, action, value):
        action.set_state(value)
        self.settings.set_value("rubber-ducky", value)
        self.get_active_window().main_stack.set_visible_child_name("emotions")
        if value:
            self.get_active_window().main_stack.set_visible_child_name("rubber-ducky")

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.get_active_window()
        if not win:
            win = ListenerWindow(application=self)
        win.present()

    def __on_about_action(self, *args):
        """Callback for the app.about action."""
        about = Adw.AboutWindow.new_from_appdata(
            "/io/gitlab/gregorni/Listener/appdata.xml"
        )
        about.set_transient_for(self.get_active_window())
        about.set_artists(["Brage Fuglseth https://bragefuglseth.dev"])
        about.set_developer_name(_("Listener Contributors"))
        # These are Python lists: Add your string to the list (separated by a comma)
        # See the translator comment below for possible formats
        about.set_developers(["gregorni https://gitlab.gnome.org/gregorni"])
        about.set_copyright(_("Copyright © 2023 Listener Contributors"))
        # Translators: Translate this string as your translator credits.
        # Name only:    gregorni
        # Name + URL:   gregorni https://gitlab.gnome.org/gregorni/
        # Name + Email: gregorni <gregorniehl@web.de>
        # Do not remove existing names.
        # Names are separated with newlines.
        about.set_translator_credits(_("translator-credits"))

        about.add_acknowledgement_section(
            _("Code and Design Borrowed from"),
            [
                "Blanket https://github.com/rafaelmardojai/blanket/",
            ],
        )

        about.present()

    def __create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    return ListenerApplication().run(sys.argv)
